<?php

/***
 * Simplified snippet from Codebase
 * In the real code this factory is abstracted and injected using Dependency Injection
 *
 * @var [] $conf - configuration pulled in for the current domain.
 */
try {

    $factory = new Sage\Domain\factory\PublicationDateFinderFactory;
    $finder = $factory->get($conf['domain']);
    return $finder->find();

} catch (SageCommerceException $e) {
    // This means that there were no products with a relevant publication date.
    // If appropriate we fallback to copyright year.
    return '&copy; ' . $this->getCopyrightYear();
}

