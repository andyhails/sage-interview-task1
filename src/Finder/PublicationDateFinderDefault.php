<?php

namespace Sage\Finder;

/**
 * @file
 * SagePublicationDateFinderDefault is the default implementation for finding publication dates for a set of products,
 * it looks for certain binding types to display before ahead of others.
 */

class PublicationDateFinderDefault implements PublicationDateFinder {

    /**
     * @var \SageCommerceProduct[]
     */
    private $products;

    public function __construct(array $products) {
        array_walk($products, function (SageCommerceProduct $product) {
            $this->products[$product->getBindingTypeCode()] = $product;
        });
        $this->reorderProducts();
    }

    /**
     * This crazy little method uses array_* functions to order an array .
     * It's not very readable but it's useful.
     */
    private function reorderProducts() {
        $order = [
          SageCommerceProductBook::BINDING_CODE_PAPERBACK_ISBN,
          SageCommerceProductBook::BINDING_CODE_SPIRAL,
          SageCommerceProductBook::BINDING_CODE_HARDCOVER,
          SageCommerceProductBook::BINDING_CODE_EPUB3_ISBN,
          SageCommerceProductBook::BINDING_CODE_EPUB_ISBN,
        ];
        $this->products = array_filter(array_merge(array_fill_keys(array_flip($order), ''), $this->products));
    }

    /**
     * {@inheritdoc}
     */
    public function find() {
        if ($this->products) {
            /** @var \SageCommerceProduct $product */
            $product = reset($this->products);
            return $product->getPublicationDate() ?: '';
        }
        throw new SageCommerceException("No products with eligible publication date");
    }

}
