<?php

namespace Sage\Finder;

/**
 * Interface SagePublicationDateFinder is used to find a particular publication date
 * for a set of products.
 */
interface PublicationDateFinder {

    /**
     * @return string
     *
     * @throws \SageCommerceException
     *  If there are no eligible products for publication dates.
     */
    public function find();
}