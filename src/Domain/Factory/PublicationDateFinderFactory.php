<?php
/**
 * @file
 * Displaying Publication dates work differently per domain,
 * PublicationDateFinderFactory provides a 'finder' class for a given domain.
 */

namespace Sage\Domain\factory;


class PublicationDateFinderFactory {

    private $domain;

    public function __construct($domain = 'US') {
        $this->domain = $domain;
    }

    /**
     * @param \SageCommerceProduct[] $products
     *
     * @return \SagePublicationDateFinder
     */
    public function get(array $products) {
        if ($this->domain === 'IN') {
            return new Sage\Finder\PublicationDateFinderIndia($products);
        }
        return new Sage\Finder\PublicationDateFinderDefault($products);
    }
}
