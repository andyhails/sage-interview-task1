<?php

namespace Sage\Domain\Finder;


use Sage\Finder\PublicationDateFinder;
/**
 * @file
 * SagePublicationDateFinderIndia is the India website implementation for finding publication dates for a set of products,
 * it looks to see if a certain subset of binding types are present to determine which to show.
 */

class SagePublicationDateFinderIndia implements PublicationDateFinder {

    /**
     * @var array
     */
    private $products;

    public function __construct(array $products) {
        $this->products = array_reduce($products, function (array $products, SageCommerceProduct $product) {
            if ($product->exists() && $product->getPublicationDate()) {
                $products[$product->getBindingTypeCode()] = $product;
            }
            return $products;
        }, []);
    }

    /**
     * {@inheritdoc}
     */
    public function find() {
        if (
          isset($this->products[SageCommerceProductBook::BINDING_CODE_PAPERBACK_ISBN]) &&
          isset($this->products[SageCommerceProductBook::BINDING_CODE_HARDCOVER])
        ) {
            $products = [
              $this->products[SageCommerceProductBook::BINDING_CODE_PAPERBACK_ISBN],
              $this->products[SageCommerceProductBook::BINDING_CODE_HARDCOVER]
            ];
            usort($products, [$this, 'sortProductsByPublicationDate']);
            $product = reset($products);
            return $product->getPublicationDate();
        }

        if (isset($this->products[SageCommerceProductBook::BINDING_CODE_HARDCOVER])) {
            $product = $this->products[SageCommerceProductBook::BINDING_CODE_HARDCOVER];
            return $product->getPublicationDate();
        }

        if (isset($this->products[SageCommerceProductBook::BINDING_CODE_EPUB_ISBN])) {
            $product = $this->products[SageCommerceProductBook::BINDING_CODE_EPUB_ISBN];
            return $product->getPublicationDate();
        }
        throw new SageCommerceException("No products with eligible publication date");
    }

    /**
     * Utility method used in usort for sorting based on publication date.
     *
     * @param \SageCommerceProduct $a
     * @param \SageCommerceProduct $b
     *
     * @return int
     */
    public function sortProductsByPublicationDate(SageCommerceProduct $a, SageCommerceProduct $b) {
        if ($a->getPublicationDate() === $b->getPublicationDate()) {
            return 0;
        }
        return ($a->getPublicationDate() < $b->getPublicationDate()) ? -1 : 1;
    }

}
